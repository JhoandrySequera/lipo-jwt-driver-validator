package com.liporide.jwtDriverValidator;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.liporide.jwtDriverValidator.model.*;
import com.liporide.jwtDriverValidator.util.JWTOptions;

import java.util.Collections;
import java.util.logging.Logger;

public class JwtDriverValidatorApplication implements RequestHandler<TokenAuthorizerContext, AuthorizerResponse> {

	private final static Logger LOGGER = Logger.getLogger(JwtDriverValidatorApplication.class.getName());

	@Override
	public AuthorizerResponse handleRequest(TokenAuthorizerContext request, Context context) {
		Statement statement = getStatement(request.getMethodArn(), "Deny");

		try {
			JWTOptions.validate(request.getAuthorizationToken(), request.getMethodArn());
			statement = getStatement(request.getMethodArn(), "Allow");
		} catch (Exception e) {
			LOGGER.warning("ERROR validating JWT -> " + e.getMessage());
		}

		return AuthorizerResponse.builder()
				.principalId("driver")
				.policyDocument(
						PolicyDocument.builder()
						.statements(Collections.singletonList(statement))
						.build())
				.build();
	}

	private Statement getStatement(String arn, String effect) {
		return Statement.builder()
				.resource(arn)
				.effect(effect)
				.build();
	}
}
