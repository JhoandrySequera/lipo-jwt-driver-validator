package com.liporide.jwtDriverValidator.util;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public class JWTOptions {

    private Optional<Map<String, Object>> validateAndGetClaims(String jwtToken, String methodArn) throws FirebaseAuthException, IOException {
        FirebaseToken decodedToken = FirebaseConnection.getAuthReference(methodArn).verifyIdToken(jwtToken);
        return Optional.of(decodedToken.getClaims());
    }

    public static void validate(String jwtToken, String methodArn) throws FirebaseAuthException, IOException {
        FirebaseConnection.getAuthReference(methodArn).verifyIdToken(jwtToken);
    }
}
