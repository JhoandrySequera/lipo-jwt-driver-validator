package com.liporide.jwtDriverValidator.util;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.liporide.jwtDriverValidator.util.FirebaseCredential.DRIVER_FIREBASE_PROJECT;

public class FirebaseConnection {

    public static FirebaseAuth getAuthReference(String methodArn) throws IOException {
        FirebaseApp driverFirebaseApp = loadConfiguration(FirebaseCredential.environment(methodArn));
        return FirebaseAuth.getInstance(driverFirebaseApp);
    }

    private static FirebaseApp loadConfiguration(FirebaseCredential environment) throws IOException {
        if(FirebaseApp.getApps().isEmpty() && !isAlreadyRegisteredApp()) {
            InputStream inputStream = new ByteArrayInputStream(environment.credential().getBytes());
            FirebaseOptions firebaseOptions = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(inputStream))
                    .setDatabaseUrl(environment.dataBaseUrl()).build();

            return FirebaseApp.initializeApp(firebaseOptions, DRIVER_FIREBASE_PROJECT);
        }

        return FirebaseApp.getInstance(DRIVER_FIREBASE_PROJECT);
    }

    private static boolean isAlreadyRegisteredApp(){
        return FirebaseApp.getApps().stream().anyMatch(app -> app.getName().equalsIgnoreCase(DRIVER_FIREBASE_PROJECT));
    }
}
