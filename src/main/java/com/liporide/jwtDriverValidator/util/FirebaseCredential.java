package com.liporide.jwtDriverValidator.util;

public enum FirebaseCredential {

    DEVELOP {
        @Override public String environmentName() { return "develop"; }

        @Override
        public String dataBaseUrl() {
            return "https://cabbieone-debug.firebaseio.com";
        }

        @Override
        public String credential() {
            return "{\n" +
                    "  \"type\": \"service_account\",\n" +
                    "  \"project_id\": \"cabbieone-debug\",\n" +
                    "  \"private_key_id\": \"55744835c64afec6e675ef8b34b2c2ffb82cec39\",\n" +
                    "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCkPEKuJO1eqShu\\ngeZcUlDuVxxMDxThk3hYNOFi9Ed4xxv+lDq7gOqm5OdUTHwjH3gvlLePsH530Y7y\\n/o8aiSLZ0hDfP5y7iPdllPp9V5j3RnTi6pHHmrR78yI3BIQM3UWY6FclMsD/O4I/\\nP2mvTPoPYThbauyEkS7FmaSNp4pl2VByet5RMTVGMMoyxcOOv/ij6nvToN6o92oi\\njJEOEFLvvzVBIOlQv5myBOqCh07vsp/aezHLV1ATmXRAt/kzq79Nq125q0LSv+Gw\\nNWz6aVbs+ElclKnRPgs7lzctyXwtZ+ppbtrp1BahJd/Ut7QNzvY+K9et8x+UU/fw\\njYduJ3oJAgMBAAECggEAFZmMToFyS4sVU3VXoeg8lyATS2iHX0SJJRatVtmYRcjw\\nBFsj31Gs6s67ouFdSTREFwPo4We9tk30Dj7O/Ncw43NmfyoLV7nDidNeX1pENq9I\\nQ6jEi/srhgq7wv8fy95VHy9deLm0Ws0Ni/pv0Juf3qJzakiFe0jp3RigJPY2ND86\\nzbmgFPU928hL9bmYdvrftXEbc52MD05if5FzL3b7goBZNVbwyQuJookGp9nL0LDd\\nre+qaj83yFxt5hXfmTXeOl/8PgtNbeo+TMqf0n/5eOXTNJqBUr2VQnEIZ/0Zjfa8\\neDdsJi8XOuHQ5A1kmYf6wV4wQokrCAvzYw/7WwxCdQKBgQDQBroatOrMV0rvLv3W\\ngtiGut1t5B5uQJ1NHfReoX8ivN2A39nxaeaqkvAutXjFXWNzfW0hGXYertnfHa8M\\nl21TQCW36N5stMm5k5ZYLrKD73viDzO6pbZ13SodwiPgUgcjIegL/hvwnqr3e90J\\ngu0iCWljZU908eztL6rWA3QVrQKBgQDKHD7hTxZldRylh7YCudhZ7MTiX10GiRWI\\nP0rmbeYRI7OFU/0x5K86g9+0TDkOiwjaSm/h37YzLkFzIPo3gZh8Ebu2/ytgFh2I\\npXS7UWOtWv2XmwaKo8yMkbftjxhGiBhoFteuOWrW5QIJ80DsDNvOVG/0vbzMigDy\\nacL7dd1pTQKBgESC8qf5DZte5H8BqvgK/+Tfhi27A4qsHSJYyzKfXY/DQ/A9yTmI\\n9z8WRaQcgDoAF9Dhbh+Iky5nx7mDvTMYbBkO910qhJaotAc6ZDmGp6RsKxC4kQDX\\nss/+lvBWEBrA6TjY4eORPQJyPzxivtlzFUYwc1UY+KI7SxySpmD1ojXBAoGBAJaG\\nluZjasDBikZWXt6E33eJ+dDHSZdWfaztxrD75L+bIVcJiKeiNEeWdAPqSQxlzIAX\\ntGA7p41Sw/gk9XZllNXCt1XBkeKMVg6xe9EO+vffrsM8+JBFElEXLi9FxcSyE8ti\\nR9xAcyv9f5k9jHrB0OtIw+E5duW3WqEw18FEtVR5AoGAfLdi+Zm4/xSSLODSsdpm\\nbthgHXkFcn3z1Q7DhZEn8axWvOs11iNYLD5cN8QTtdXBFGnO4qnLzNc7G7fWnFLV\\ndpYnOrx8EiQkkxhzHOXA28JobtpWAbGI29QJP3EEXVQNmyg30jk9vij6vytuLyok\\n2fmI+KjkaNw3dNfQc2dJw8o=\\n-----END PRIVATE KEY-----\\n\",\n" +
                    "  \"client_email\": \"firebase-adminsdk-5tywq@cabbieone-debug.iam.gserviceaccount.com\",\n" +
                    "  \"client_id\": \"102936049651845834164\",\n" +
                    "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                    "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                    "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                    "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-5tywq%40cabbieone-debug.iam.gserviceaccount.com\"\n" +
                    "}\n";
        }
    },
    STAGE {
        @Override public String environmentName() { return "stage"; }

        @Override
        public String dataBaseUrl() {
            return null;
        }

        @Override
        public String credential() {
            return null;
        }
    },
    PRODUCTION {
        @Override public String environmentName() { return "production"; }

        @Override
        public String dataBaseUrl() {
            return "https://lipo-driver-production.firebaseio.com";
        }

        @Override
        public String credential() {
             return "{\n" +
                     "  \"type\": \"service_account\",\n" +
                     "  \"project_id\": \"lipo-driver-production\",\n" +
                     "  \"private_key_id\": \"024db4176058399f4c11bce074b9da72642132fa\",\n" +
                     "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsUheBF5cu6gvm\\nZYl+4KqMaKQ8c6GwR4pkT6B7U4/aDXUxBkhQBV+djUNT9ofbUcfaEW0yKep4Gpqg\\nzaoNWTvSW5oYUjM6fcFqMGR3WHPKICw39t8zgZ0Ll2cYyt9OQuo59mONYrAJuAqo\\naJgsXlbpYSpmKU4vMspwzWgnqLIPD2Q/H0QlKveotqkONCqwmeE9vTnPePaLbIri\\n64H+rFQddKkCzZTNzTR7GmQh074NRgoUHVvratxFBaJuFlUXb55TlOC5ijPQkYQo\\nkoeH3/9+UFUt7Mw+oTmJDGKSb3fxRSz8cqTtdCGHD3dyr1YcaTnjXm+UMv4qp1l6\\nUGzUFKk3AgMBAAECggEAAIr/rPX0kX7GS9MNFFvD3c5kmhYpL8OB8Z/4mvrf7Kl3\\nMZDaAHqL2xS7XhPcSLogvZSpeet85CT9UL1lKEacNCT6vtHffHMx/VIqtt+N+diR\\nGxoJiQGDC6rN9duJyZbXYxheJu9lK5qrWe1+cUFY18JpHkpKB0cLMUDMb7eN3BNI\\ngM2Wve19ktVqxqzOjMhYYaxvPoP+Z/CzQKP86QYHCWkaa3forUzggOD/9sXuo2+p\\nwOk1ghqyQcEF+lY9yL4Nc5897m5y15viKsnRLGzqyRLOOjW1KYNrlo1BNwnPhJH6\\n8upNGc2kEDJy9ShEZ9DQei2WaPSFVEvEhS58Se0dQQKBgQDyIy+ixY1jKpD6kjX4\\n2A+jrdAnCo8v2SXlk45/KTKmunCruTofFskR1AauaqQkEQta5+DoyMFhLQl7o8ze\\nxr5HEP/l2ODoZtUPiBacuyILIKrhxqmYXql0rMMh9HPWuex2Ks6Ym/wG7BVcVqwv\\n3bM8/xfNAArdShqKzoSBG7eV9wKBgQC2L6hKksZ9J/+8AcdeXVGhVeNB8KC7AD73\\n8rT3qYr1XeIbkk+3KQCo2AqbV9tpLhBMhilVnSPDraR27LogLPGQsK0Wjl6SoADq\\nzl9zfKsY8usfFYWvkmJCGxYX9pckxepbNb9NRgMd59Lc/fsDbirG+UVJREgCHI5G\\nXbAQZqm2wQKBgDxknksA7wTe5w44TZHlgfEXNyWlspUCmjsqlI4O3n1LXzLVnjOG\\n4Vu7AHecTZhZT/W6hBRLjkAIlabq8fxWobrj+I+BiocsrtOS4+Dvo8wWt8hcXoyM\\nf8OEG59qc0IJWXsXrjPOJAwt8h332DaQG1aJYYxWCF+tTfV1EQOKwD19AoGAc4TG\\nbfqhNuys4DY0hyYew3DlN4NvNwOR6cAQdVUgkjJkU10zhIzQKT9LgnJz4j5eAVe0\\nT9/itIme1fWGky5MQZV1Ou8tEkW4LyqKJX0wNBKbeUs5SDlGamooWSt3bM69qfXr\\nhgDVqxm6f3vftK4fG6klAK72Bbi7keyH3e5qwEECgYBgQiM8MKk54BNW1FM9GKuB\\nGWUoiABGPqrTwAetVgJgNpyM/Z72qkMktr2mIGE/zI12Ce6gkSoVZs4zVaeAiXyP\\nV61w2iEnImc2f7jxhqADSYW/3Vo5Wf2RUt+LH9h+CqxpZ5z8gNT2pd+tXjqxmU1g\\nviyYOc7WYlGh4mr/A/a45w==\\n-----END PRIVATE KEY-----\\n\",\n" +
                     "  \"client_email\": \"firebase-adminsdk-c13fh@lipo-driver-production.iam.gserviceaccount.com\",\n" +
                     "  \"client_id\": \"101462927123952820703\",\n" +
                     "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                     "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                     "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                     "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-c13fh%40lipo-driver-production.iam.gserviceaccount.com\"\n" +
                     "}\n";
        }
    };

    public static final String DRIVER_FIREBASE_PROJECT = "driver";

    public abstract String environmentName();
    public abstract String dataBaseUrl();
    public abstract String credential();

    public static FirebaseCredential environment(String methodArn) {
        for (FirebaseCredential credential : FirebaseCredential.values()) {
            if(methodArn.contains(credential.environmentName())) {
                return credential;
            }
        }

        throw new NullPointerException("Cant get environment credentials");
    }
}
